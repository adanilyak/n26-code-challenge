//
//  AppDelegate.swift
//  N26CodeChallenge
//
//  Created by a.danilyak on 30/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    /// Main Application Window
    var window: UIWindow?
    
    /// Assembler for creating all screens
    private var assembler: IAssembler?
    
    /// Roter for all screens
    private var router: IRouter?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        assembler = Assembler()
        
        guard let assembler = assembler else { return false }
        router = Router(assembler: assembler)
        
        guard let router = router else { return false }
        router.routeToHistoryExchangeRatesViewController(animated: false)
        
        window = createWindow(with: router.rootController)
        window?.makeKeyAndVisible()
        
        return true
    }
}

// MARK: - Window Creation
extension AppDelegate {
    
    /// Create default application window
    ///
    /// - Parameter rootController: root controller from router
    /// - Returns: window
    private func createWindow(with rootController: UIViewController) -> UIWindow {
        let window = UIWindow()
        window.backgroundColor = .white
        window.rootViewController = rootController
        return window
    }
}
