//
//  Assembler.swift
//  N26CodeChallenge
//
//  Created by a.danilyak on 30/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import Foundation
import N26CodeChallengeCore

/// Implementation for assembler
final class Assembler: IAssembler {

    // Services
    lazy var apiService: IAPIService = APIService(configuration: .default)
    lazy var exchangeRateService: IExchangeRateService = ExchangeRateService(apiService: self.apiService)
}

// MARK: - HistoryExchangeRatesModuleAssembler
extension Assembler: HistoryExchangeRatesModuleAssembler {
    
    func assembleHistoryExchangeRatesViewController(router: HistoryExchangeRatesModuleRouter) -> HistoryExchangeRatesViewController {
        let presenter = HistoryExchangeRatesPresenter(exchangeRatesService: exchangeRateService)
        return HistoryExchangeRatesViewController(presenter: presenter, router: router)
    }
    
    func assembleSingleExchangeRateViewController(date: Date, exchangeRates: [ExchangeRate]) -> SingleExchangeRateViewController {
        return SingleExchangeRateViewController(date: date, exchangeRates: exchangeRates)
    }
}
