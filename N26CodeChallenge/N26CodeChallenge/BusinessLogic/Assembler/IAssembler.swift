//
//  IAssembler.swift
//  N26CodeChallenge
//
//  Created by a.danilyak on 30/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import Foundation
import N26CodeChallengeCore

/// Main application assembler, handles all screens creation
protocol IAssembler: HistoryExchangeRatesModuleAssembler {
    
    // Services
    var apiService: IAPIService { get }
    var exchangeRateService: IExchangeRateService { get }
}
