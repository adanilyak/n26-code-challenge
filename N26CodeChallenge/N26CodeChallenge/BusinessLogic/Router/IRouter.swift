//
//  IRouter.swift
//  N26CodeChallenge
//
//  Created by a.danilyak on 30/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import UIKit

/// Main application router, handles all screen routing
protocol IRouter: HistoryExchangeRatesModuleRouter {
    var rootController: UINavigationController { get }
}
