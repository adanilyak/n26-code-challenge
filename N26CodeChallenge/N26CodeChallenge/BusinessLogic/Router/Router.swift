//
//  Router.swift
//  N26CodeChallenge
//
//  Created by a.danilyak on 30/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import UIKit
import N26CodeChallengeCore

/// Implementation of router
final class Router: IRouter {
    
    // Root View Controller
    let rootController = UINavigationController()
    
    // Dependencies
    let assembler: IAssembler
    
    init(assembler: IAssembler) {
        self.assembler = assembler
    }
}

// MARK: - HistoryExchangeRatesModuleRouter
extension Router: HistoryExchangeRatesModuleRouter {
    
    func routeToHistoryExchangeRatesViewController(animated: Bool) {
        let vc = assembler.assembleHistoryExchangeRatesViewController(router: self)
        rootController.pushViewController(vc, animated: animated)
    }
    
    func routeToSingleExchangeRateViewController(date: Date, exchangeRates: [ExchangeRate], animated: Bool) {
        let vc = assembler.assembleSingleExchangeRateViewController(date: date, exchangeRates: exchangeRates)
        rootController.pushViewController(vc, animated: animated)
    }
}
