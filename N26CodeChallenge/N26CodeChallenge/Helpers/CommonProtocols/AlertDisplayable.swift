//
//  AlertDisplayable.swift
//  N26CodeChallenge
//
//  Created by a.danilyak on 31/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import UIKit

protocol AlertDisplayable where Self: UIViewController {
    
    func displayError(_ errorText: String)
    func displayError(_ errorText: String, retryAction: (() -> Void)?)
}

extension AlertDisplayable {
    
    func displayError(_ errorText: String) {
        displayError(errorText, retryAction: nil)
    }
    
    func displayError(_ errorText: String, retryAction: (() -> Void)?) {
        let alert = UIAlertController(title: loc("error"),
                                      message: errorText,
                                      preferredStyle: .alert)
        let ok = UIAlertAction(title: loc("ok"),
                               style: .cancel,
                               handler: nil)
        alert.addAction(ok)
        
        if let retryAction = retryAction {
            let retry = UIAlertAction(title: loc("try_again"),
                                      style: .default,
                                      handler: { _ in retryAction() })
            alert.addAction(retry)
        }
        
        present(alert, animated: true, completion: nil)
    }
}
