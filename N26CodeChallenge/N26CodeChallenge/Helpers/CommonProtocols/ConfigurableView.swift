//
//  ConfigurableView.swift
//  N26CodeChallenge
//
//  Created by a.danilyak on 31/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import UIKit

protocol ConfigurableView where Self: UIView {
    associatedtype Model
    func configure(_ model: Model)
}
