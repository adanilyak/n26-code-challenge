//
//  Localization.swift
//  N26CodeChallenge
//
//  Created by a.danilyak on 31/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import Foundation

func loc(_ key: String) -> String {
    return NSLocalizedString(key, comment: "")
}
