//
//  HistoryExchangeRatesModuleAssembler.swift
//  N26CodeChallenge
//
//  Created by a.danilyak on 31/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import Foundation
import N26CodeChallengeCore

protocol HistoryExchangeRatesModuleAssembler: AnyObject {
    
    /// Creates controller with history exchange rates and today rate
    ///
    /// - Parameter router: router
    /// - Returns: controller
    func assembleHistoryExchangeRatesViewController(router: HistoryExchangeRatesModuleRouter) -> HistoryExchangeRatesViewController
    
    /// Creates controller with single day exhchage rates
    ///
    /// - Parameters:
    ///   - date: date
    ///   - exchangeRates: rates in different currencies
    /// - Returns: controller
    func assembleSingleExchangeRateViewController(date: Date, exchangeRates: [ExchangeRate]) -> SingleExchangeRateViewController
}
