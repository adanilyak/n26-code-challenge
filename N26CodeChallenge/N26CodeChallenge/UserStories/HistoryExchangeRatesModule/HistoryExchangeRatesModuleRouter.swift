//
//  HistoryExchangeRatesModuleRouter.swift
//  N26CodeChallenge
//
//  Created by a.danilyak on 31/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import Foundation
import N26CodeChallengeCore

protocol HistoryExchangeRatesModuleRouter: AnyObject {
    
    /// Route to controller with history exchange rates and today rate
    ///
    /// - Parameter animated: animated
    func routeToHistoryExchangeRatesViewController(animated: Bool)
    
    /// Route to controller with single day exhchage rates
    ///
    /// - Parameters:
    ///   - date: date
    ///   - exchangeRates: rates in different currencies
    ///   - animated: animated
    func routeToSingleExchangeRateViewController(date: Date, exchangeRates: [ExchangeRate], animated: Bool)
}
