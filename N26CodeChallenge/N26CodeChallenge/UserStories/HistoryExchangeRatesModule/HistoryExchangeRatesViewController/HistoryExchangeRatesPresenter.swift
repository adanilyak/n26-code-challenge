//
//  HistoryExchangeRatesPresenter.swift
//  N26CodeChallenge
//
//  Created by a.danilyak on 31/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import Foundation
import N26CodeChallengeCore

/// Presenter for HistoryExchangeRates controller
protocol IHistoryExchangeRatesPresenter: AnyObject {
    var view: IHistoryExchangeRatesView? { get set }
    var viewModel: HistoryExchangeRatesViewModel { get }
    func startUpdateTodayExchangeRate()
    func stopUpdateTodayExchangeRate()
    func updateTodayExchangeRate()
    func updateHistoryExchangeRates()
}

/// ViewModel for HistoryExchangeRates controller
struct HistoryExchangeRatesViewModel {
    var todayExchangeRate: ExchangeRate?
    
    var displayHistoryExchangeRates: [ExchangeRate] = []
    var historyExchangeRates: [Date: [ExchangeRate]] = [:]
}

/// Implementation of presenter for HistoryExchangeRates controller
final class HistoryExchangeRatesPresenter: IHistoryExchangeRatesPresenter {
    
    // Properties
    weak var view: IHistoryExchangeRatesView?
    var viewModel: HistoryExchangeRatesViewModel
    private var todayUpdateTimer: Timer?
    
    // Dependencies
    let exchangeRatesService: IExchangeRateService
    
    // MARK: - Init
    
    init(exchangeRatesService: IExchangeRateService) {
        self.viewModel = HistoryExchangeRatesViewModel()
        self.exchangeRatesService = exchangeRatesService
    }
    
    // MARK: - IHistoryExchangeRatesPresenter
    
    func startUpdateTodayExchangeRate() {
        todayUpdateTimer = Timer.scheduledTimer(withTimeInterval: 60, repeats: true) { [weak self] _ in
            self?.updateTodayExchangeRate()
        }
    }
    
    func stopUpdateTodayExchangeRate() {
        todayUpdateTimer?.invalidate()
        todayUpdateTimer = nil
    }
    
    func updateTodayExchangeRate() {
        exchangeRatesService.loadBitcoinExchangeRates { [weak self] result in
            switch result {
            case .success(let rates):
                self?.viewModel.todayExchangeRate = rates.first(where: { $0.to == .eur })
                DispatchQueue.main.async { self?.view?.todayExchangeRateDidUpdated() }
            case .failure:
                DispatchQueue.main.async { self?.view?.displayError(loc("history_exchange_rates_cant_load_today")) }
            }
        }
    }
    
    func updateHistoryExchangeRates() {
        let today = Date()
        guard let twoWeeksAgo = Calendar.current.date(byAdding: .day, value: -14, to: today) else { return }
        exchangeRatesService.loadHistoricalBitcoinExchangeRates(from: twoWeeksAgo, to: today, with: [.eur, .usd, .gbp]) { [weak self] result in
            switch result {
            case .success(let rates):
                self?.viewModel.historyExchangeRates = rates
                self?.viewModel.displayHistoryExchangeRates = rates.values
                    .compactMap { $0.first(where: { $0.to == .eur }) }
                    .sorted(by: { $0.date > $1.date })
                DispatchQueue.main.async { self?.view?.historyExchangeRatesDidUpdated() }
            case .failure:
                DispatchQueue.main.async {
                    self?.view?.displayError(loc("history_exchange_rates_cant_load_history")) {
                        self?.updateHistoryExchangeRates()
                    }
                }
            }
        }
    }
}
