//
//  HistoryExchangeRatesViewController.swift
//  N26CodeChallenge
//
//  Created by a.danilyak on 31/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import UIKit
import SnapKit

protocol IHistoryExchangeRatesView: AlertDisplayable {
    func todayExchangeRateDidUpdated()
    func historyExchangeRatesDidUpdated()
}

final class HistoryExchangeRatesViewController: UIViewController {
    
    // Dependencies
    private let presenter: IHistoryExchangeRatesPresenter
    private weak var router: HistoryExchangeRatesModuleRouter?
    private let viewModelFactory = HistoryExchangeViewModelFactory()
    
    // UI
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(HistoryExchangeRateCell.self, forCellReuseIdentifier: .historyExchangeCellId)
        
        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(self.onRefresh), for: .valueChanged)
        tableView.refreshControl = refresh
        
        return tableView
    }()
    
    // MARK: - Init
    
    init(presenter: IHistoryExchangeRatesPresenter, router: HistoryExchangeRatesModuleRouter) {
        self.presenter = presenter
        self.router = router
        super.init(nibName: nil, bundle: nil)
        self.presenter.view = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
        presenter.updateTodayExchangeRate()
        
        tableView.refreshControl?.beginRefreshing()
        presenter.updateHistoryExchangeRates()
        
        presenter.startUpdateTodayExchangeRate()
    }
    
    deinit {
        presenter.stopUpdateTodayExchangeRate()
    }
    
    // MARK: - Setup
    
    private func setupUI() {
        title = loc("history_exchange_rates_title")
        
        view.addSubview(tableView)
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    @objc private func onRefresh() {
        presenter.updateHistoryExchangeRates()
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension HistoryExchangeRatesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0: return presenter.viewModel.todayExchangeRate == nil ? 0 : 1
        case 1: return presenter.viewModel.displayHistoryExchangeRates.count
        default: return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = HistoryExchangeRateCell(style: .subtitle, reuseIdentifier: .historyExchangeCellId)
        
        switch indexPath.section {
        case 0:
            guard let rate = presenter.viewModel.todayExchangeRate else { break }
            cell.configure(viewModelFactory.buildHistoryExchangeRateCellModel(from: rate, isTimeNeeded: true))
        case 1:
            let model = viewModelFactory
                .buildHistoryExchangeRateCellModel(from: presenter.viewModel.displayHistoryExchangeRates[indexPath.row])
            cell.configure(model)
        default: break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard indexPath.section == 1 else { return }
        let date = presenter.viewModel.displayHistoryExchangeRates[indexPath.row].date
        let exchangeRates = presenter.viewModel.historyExchangeRates[date]
        router?.routeToSingleExchangeRateViewController(date: date, exchangeRates: exchangeRates ?? [], animated: true)
    }
}

// MARK: - IHistoryExchangeRatesView
extension HistoryExchangeRatesViewController: IHistoryExchangeRatesView {
    
    func todayExchangeRateDidUpdated() {
        tableView.beginUpdates()
        tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
        tableView.endUpdates()
    }
    
    func historyExchangeRatesDidUpdated() {
        tableView.refreshControl?.endRefreshing()
        tableView.beginUpdates()
        tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
        tableView.endUpdates()
    }
}

// MARK: - Constants
private extension String {
    static let historyExchangeCellId = "historyExchangeCellId"
}
