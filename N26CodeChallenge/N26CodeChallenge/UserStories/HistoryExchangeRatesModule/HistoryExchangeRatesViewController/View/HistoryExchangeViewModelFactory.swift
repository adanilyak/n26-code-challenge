//
//  HistoryExchangeViewModelFactory.swift
//  N26CodeChallenge
//
//  Created by a.danilyak on 31/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import UIKit
import N26CodeChallengeCore

final class HistoryExchangeViewModelFactory {
    
    func buildHistoryExchangeRateCellModel(from exchangeRate: ExchangeRate, isTimeNeeded: Bool = false) -> HistoryExchangeRateCell.Model {
        let title = "\(exchangeRate.rate) \(exchangeRate.to.symbol ?? "")"
        let subtitle = isTimeNeeded
            ? DateFormatter.ddMMyyyyHHmm.string(from: exchangeRate.date)
            : DateFormatter.ddMMyyyy.string(from: exchangeRate.date)
        return HistoryExchangeRateCell.Model(title: title, subtitle: subtitle)
    }
}

// MARK: - Constants
private extension DateFormatter {
    
    static let ddMMyyyyHHmm: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy HH:mm"
        formatter.timeZone = Calendar.current.timeZone
        return formatter
    }()
    
    static let ddMMyyyy: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        formatter.timeZone = Calendar.current.timeZone
        return formatter
    }()
}
