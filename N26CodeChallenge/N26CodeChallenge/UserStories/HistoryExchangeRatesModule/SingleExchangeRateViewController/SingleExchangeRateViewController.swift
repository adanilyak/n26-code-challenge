//
//  SingleExchangeRateViewController.swift
//  N26CodeChallenge
//
//  Created by a.danilyak on 31/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import UIKit
import SnapKit
import N26CodeChallengeCore

final class SingleExchangeRateViewController: UIViewController {
    
    // UI
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(HistoryExchangeRateCell.self, forCellReuseIdentifier: .historyExchangeCellId)
        
        return tableView
    }()
    
    // Dependencies
    private let viewModelFactory = HistoryExchangeViewModelFactory()
    
    // Properties
    private let exchangeRates: [ExchangeRate]
    private let date: Date
    
    // MARK: - Init
    
    init(date: Date, exchangeRates: [ExchangeRate]) {
        self.date = date
        self.exchangeRates = exchangeRates
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = DateFormatter.ddMMyyyy.string(from: date)
        
        view.addSubview(tableView)
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension SingleExchangeRateViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exchangeRates.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = HistoryExchangeRateCell(style: .subtitle, reuseIdentifier: .historyExchangeCellId)
        
        cell.configure(viewModelFactory.buildHistoryExchangeRateCellModel(from: exchangeRates[indexPath.row]))
        
        return cell
    }
}

// MARK: - Constants
private extension String {
    static let historyExchangeCellId = "historyExchangeCellId"
}

private extension DateFormatter {
    
    static let ddMMyyyy: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        formatter.timeZone = Calendar.current.timeZone
        return formatter
    }()
}
