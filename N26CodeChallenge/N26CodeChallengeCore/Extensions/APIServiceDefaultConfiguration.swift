//
//  APIServiceDefaultConfiguration.swift
//  N26CodeChallengeCore
//
//  Created by a.danilyak on 31/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import Foundation

public extension APIServiceConfiguration {
    static let `default` = APIServiceConfiguration(scheme: "https", host: "api.coindesk.com", apiVersion: "v1")
}
