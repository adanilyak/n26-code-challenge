//
//  JSON+Decimal.swift
//  N26CodeChallenge
//
//  Created by a.danilyak on 30/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import Foundation
import SwiftyJSON

extension JSON {
    
    var decimal: Decimal? {
        guard let double = double else { return nil }
        return Decimal(double)
    }
}
