//
//  Currency.swift
//  N26CodeChallenge
//
//  Created by a.danilyak on 30/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct Currency: Hashable, Equatable {
    
    /// Currency code, ex. USD
    public let code: String
    
    /// Currency symbol, ex. $
    public let symbol: String?
    
    // MARK: - Init
    
    public init(code: String) {
        self.code = code
        self.symbol = Constants.currencyCodes[code.uppercased()]
    }
}

// MARK: - Default currencies
public extension Currency {
    static let eur = Currency(code: "EUR")
    static let usd = Currency(code: "USD")
    static let gbp = Currency(code: "GBP")
    static let btc = Currency(code: "BTC")
}

/// MARK: - Constants
private struct Constants {
    static let currencyCodes = ["EUR": "€", "USD": "$", "GBP": "£", "BTC": "₿"]
}
