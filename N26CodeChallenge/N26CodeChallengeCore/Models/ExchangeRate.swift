//
//  ExchangeRate.swift
//  N26CodeChallenge
//
//  Created by a.danilyak on 30/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import Foundation
import SwiftyJSON

public struct ExchangeRate: Hashable, Equatable {
    
    public let from: Currency
    public let to: Currency
    
    /// Price
    public let rate: Decimal
    
    /// Date when rate is actual
    public let date: Date
}
