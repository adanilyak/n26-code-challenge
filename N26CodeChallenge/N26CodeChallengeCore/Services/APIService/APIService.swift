//
//  APIService.swift
//  N26CodeChallenge
//
//  Created by a.danilyak on 30/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import Foundation

public final class APIService: IAPIService {
    
    // Properties
    public let configuration: APIServiceConfiguration
    public let urlSession: URLSession
    
    // MARK: - Init
    
    public init(configuration: APIServiceConfiguration) {
        self.configuration = configuration
        self.urlSession = URLSession(configuration: .default)
    }
    
    // MARK: - Creation Methods
    
    public func createURL(pathComponents: [String], queryParams: APIServiceQueryParams) -> URL? {
        var urlComponents = URLComponents()
        urlComponents.scheme = configuration.scheme
        urlComponents.host = configuration.host
        
        if let apiVersion = configuration.apiVersion {
            urlComponents.path = "/" + apiVersion
        }
        
        if !pathComponents.isEmpty {
            urlComponents.path += "/" + pathComponents.joined(separator: "/")
        }
        
        if !queryParams.isEmpty {
            urlComponents.queryItems = queryParams.map { URLQueryItem(name: $0.key, value: $0.value) }
        }
        
        return urlComponents.url
    }
    
    public func createRequest(url: URL, method: HTTPMethod) -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        return urlRequest
    }
    
    public func performURLRequest(urlRequest: URLRequest,
                                  completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        let dataTask = urlSession.dataTask(with: urlRequest, completionHandler: completionHandler)
        dataTask.resume()
    }
}
