//
//  IAPIService.swift
//  N26CodeChallenge
//
//  Created by a.danilyak on 30/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import Foundation

public struct APIServiceConfiguration {
    public let scheme: String
    public let host: String
    public let apiVersion: String?
    
    public init(scheme: String, host: String, apiVersion: String?) {
        self.scheme = scheme
        self.host = host
        self.apiVersion = apiVersion
    }
}

public enum APIServiceError: Error {
    case invalidURL
    case serialization
}

public enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}

public typealias APIServiceQueryParams = [String: String]

public protocol IAPIService {
    
    var configuration: APIServiceConfiguration { get }
    var urlSession: URLSession { get }
    init(configuration: APIServiceConfiguration)
    
    func createURL(pathComponents: [String], queryParams: APIServiceQueryParams) -> URL?
    func createRequest(url: URL, method: HTTPMethod) -> URLRequest
    func performURLRequest(urlRequest: URLRequest,
                           completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void)
}

public extension IAPIService {
    
    func createURL(pathComponents: [String]) -> URL? {
        return createURL(pathComponents: pathComponents, queryParams: [:])
    }
}
