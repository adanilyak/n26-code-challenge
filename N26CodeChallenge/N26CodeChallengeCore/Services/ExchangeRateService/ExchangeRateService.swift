//
//  ExchangeRateService.swift
//  N26CodeChallenge
//
//  Created by a.danilyak on 30/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import Foundation
import SwiftyJSON

public final class ExchangeRateService: IExchangeRateService {

    // Dependencies
    private let apiService: IAPIService
    
    // MARK: - Init
    
    public init(apiService: IAPIService) {
        self.apiService = apiService
    }
    
    // MARK: - IExchangeRateService
    
    public func loadBitcoinExchangeRates(completion: @escaping (Result<[ExchangeRate], Error>) -> Void) {
        guard let url = apiService.createURL(pathComponents: ["bpi", "currentprice.json"]) else {
            completion(.failure(APIServiceError.invalidURL))
            return
        }
        
        let urlRequest = apiService.createRequest(url: url, method: .get)
        apiService.performURLRequest(urlRequest: urlRequest) { (data, response, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let data = data,
                let json = try? JSON(data: data),
                let isoDate = json["time"]["updatedISO"].string?.isoDate,
                let exchangeRatesDictionary = json["bpi"].dictionary
                else {
                    completion(.failure(APIServiceError.serialization))
                    return
            }
            
            let exchangeRates = exchangeRatesDictionary.compactMap { (key, value) -> ExchangeRate? in
                guard let toCode = value["code"].string, let rate = value["rate_float"].decimal else { return nil }
                return ExchangeRate(from: .btc, to: Currency(code: toCode), rate: rate, date: isoDate)
            }
            
            completion(.success(exchangeRates))
        }
    }
    
    public func loadHistoricalBitcoinExchangeRates(from startDate: Date,
                                            to endDate: Date,
                                            with currency: Currency,
                                            completion: @escaping (Result<[Date: ExchangeRate], Error>) -> Void) {
        guard let url = apiService.createURL(pathComponents: ["bpi", "historical", "close.json"],
                                             queryParams: ["start": startDate.yyyyMMdd, "end": endDate.yyyyMMdd, "currency": currency.code]) else {
            completion(.failure(APIServiceError.invalidURL))
            return
        }
        
        let urlRequest = apiService.createRequest(url: url, method: .get)
        apiService.performURLRequest(urlRequest: urlRequest) { (data, response, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            
            guard let data = data,
                let json = try? JSON(data: data),
                let exchangeRatesDictionary = json["bpi"].dictionary
                else {
                    completion(.failure(APIServiceError.serialization))
                    return
            }
            
            let exchangeRates = exchangeRatesDictionary.reduce(into: [Date: ExchangeRate](), { (result, keyValue) in
                guard let date = keyValue.key.yyyyMMdd, let rate = keyValue.value.decimal else { return }
                result[date] = ExchangeRate(from: .btc, to: currency, rate: rate, date: date)
            })
            
            completion(.success(exchangeRates))
        }
    }
    
    public func loadHistoricalBitcoinExchangeRates(from startDate: Date,
                                            to endDate: Date,
                                            with currencies: [Currency],
                                            completion: @escaping (Result<[Date: [ExchangeRate]], Error>) -> Void) {
        var results = [Result<[Date: ExchangeRate], Error>]()
        
        let group = DispatchGroup()
        currencies.forEach {
            group.enter()
            loadHistoricalBitcoinExchangeRates(from: startDate, to: endDate, with: $0, completion: { result in
                results.append(result)
                group.leave()
            })
        }
        
        group.notify(queue: .global(qos: .userInitiated)) {
            let data: [[Date: ExchangeRate]] = results.compactMap {
                switch $0 {
                case .failure: return nil
                case .success(let dateRates): return dateRates
                }
            }
            
            guard data.count == results.count else {
                completion(.failure(APIServiceError.serialization))
                return
            }
            
            var resultDateRates = [Date: [ExchangeRate]]()
            data.forEach { dateRates in
                dateRates.forEach {
                    if resultDateRates[$0.key] != nil {
                        resultDateRates[$0.key]?.append($0.value)
                    } else {
                        resultDateRates[$0.key] = [$0.value]
                    }
                }
            }
            
            completion(.success(resultDateRates))
        }
    }
}

// MARK: - Constants
private extension DateFormatter {
    
    static let yyyyMMddFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    static let isoFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return formatter
    }()
}

private extension Date {
    
    var yyyyMMdd: String {
        return DateFormatter.yyyyMMddFormatter.string(from: self)
    }
    
    var iso: String {
        return DateFormatter.isoFormatter.string(from: self)
    }
}

private extension String {
    
    var yyyyMMdd: Date? {
        return DateFormatter.yyyyMMddFormatter.date(from: self)
    }
    
    var isoDate: Date? {
        return DateFormatter.isoFormatter.date(from: self)
    }
}
