//
//  IExchangeRateService.swift
//  N26CodeChallenge
//
//  Created by a.danilyak on 30/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import Foundation

public protocol IExchangeRateService: AnyObject {
    
    /// Load today exchange rates for Bitcoin
    ///
    /// - Parameter completion: result completion
    func loadBitcoinExchangeRates(completion: @escaping (Result<[ExchangeRate], Error>) -> Void)
    
    /// Load historical Bitcoin exchange rates data for period with currency
    ///
    /// - Parameters:
    ///   - startDate: start of period
    ///   - endDate: end of period
    ///   - currency: currency
    ///   - completion: result completion
    func loadHistoricalBitcoinExchangeRates(from startDate: Date,
                                            to endDate: Date,
                                            with currency: Currency,
                                            completion: @escaping (Result<[Date: ExchangeRate], Error>) -> Void)
    
    /// Load historical Bitcoin exchange rates data for period with currencies
    ///
    /// - Parameters:
    ///   - startDate: start of period
    ///   - endDate: end of period
    ///   - currencies: currencies
    ///   - completion: result completion
    func loadHistoricalBitcoinExchangeRates(from startDate: Date,
                                            to endDate: Date,
                                            with currencies: [Currency],
                                            completion: @escaping (Result<[Date: [ExchangeRate]], Error>) -> Void)
}
