//
//  APIServiceTests.swift
//  N26CodeChallengeCoreTests
//
//  Created by a.danilyak on 31/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import XCTest
@testable import N26CodeChallengeCore

class APIServiceTests: XCTestCase {
    
    var apiService: APIService?
    
    override func setUp() {
        apiService = APIService(configuration: APIServiceConfiguration(scheme: "https",
                                                                       host: "test.com",
                                                                       apiVersion: nil))
    }
    
    override func tearDown() {}
    
    func testCreatePlainURL() {
        let expectedURL = URL(string: "https://test.com")
        let createdURL = apiService?.createURL(pathComponents: [], queryParams: [:])
        XCTAssert(expectedURL == createdURL, "URLs should be equal")
    }
    
    func testCreateParameterizedURL() {
        let expectedURL = URL(string: "https://test.com/load?page=0&index=10")
        let createdURL = apiService?.createURL(pathComponents: ["load"], queryParams: ["page": "0", "index": "10"])
        XCTAssert(expectedURL == createdURL, "URLs should be equal")
    }
}
