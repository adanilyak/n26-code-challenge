//
//  DummyAPIService.swift
//  N26CodeChallengeCoreTests
//
//  Created by a.danilyak on 31/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import Foundation
@testable import N26CodeChallengeCore

final class DummyAPIService: IAPIService {
    
    var configuration: APIServiceConfiguration
    var urlSession: URLSession
    
    init(configuration: APIServiceConfiguration) {
        self.configuration = configuration
        self.urlSession = URLSession(configuration: .default)
    }
    
    var createURLResult: URL?
    func createURL(pathComponents: [String], queryParams: APIServiceQueryParams) -> URL? {
        return createURLResult
    }
    
    var createRequestResult: URLRequest?
    func createRequest(url: URL, method: HTTPMethod) -> URLRequest {
        guard let createRequestResult = createRequestResult else { return URLRequest(url: URL(string: "https://google.com")!) }
        return createRequestResult
    }
    
    var performURLRequestData: Data?
    var performURLRequestURLResponse: URLResponse?
    var performURLRequestError: Error?
    func performURLRequest(urlRequest: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        completionHandler(performURLRequestData, performURLRequestURLResponse, performURLRequestError)
    }
}
