//
//  ExchangeRateServiceTest.swift
//  N26CodeChallengeCoreTests
//
//  Created by a.danilyak on 31/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import XCTest
@testable import N26CodeChallengeCore

class ExchangeRateServiceTest: XCTestCase {
    
    var dummyApiService: DummyAPIService?
    var exchangeRateService: IExchangeRateService?
    
    override func setUp() {
        dummyApiService = DummyAPIService(configuration: .default)
        exchangeRateService = ExchangeRateService(apiService: dummyApiService!)
    }
    
    override func tearDown() {}
    
    func testLoadBitcoinExchangeRatesFailure() {
        dummyApiService?.performURLRequestData = nil
        dummyApiService?.performURLRequestError = APIServiceError.invalidURL
        exchangeRateService?.loadBitcoinExchangeRates(completion: { result in
            switch result {
            case .failure:
                XCTAssert(true, "Correct")
            case .success:
                XCTAssert(false, "Should be failure")
            }
        })
    }
    
    func testLoadBitcoinExchangeRatesSuccess() {
        dummyApiService?.createURLResult = URL(string: "https://test.com")!
        dummyApiService?.performURLRequestData = JSONResponses.getLoadBitcoinExchangeRatesJSON()
        dummyApiService?.performURLRequestError = nil
        exchangeRateService?.loadBitcoinExchangeRates(completion: { result in
            switch result {
            case .failure:
                XCTAssert(false, "Should be success")
            case .success:
                XCTAssert(true, "Correct")
            }
        })
    }
}
