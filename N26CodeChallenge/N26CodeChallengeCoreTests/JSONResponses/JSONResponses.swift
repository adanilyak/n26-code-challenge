//
//  JSONResponses.swift
//  N26CodeChallengeCoreTests
//
//  Created by a.danilyak on 31/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import Foundation

class JSONResponses {
    
    static func getLoadBitcoinExchangeRatesJSON() -> Data? {
        guard let path = Bundle(for: JSONResponses.self).path(forResource: "today", ofType: "json") else { return nil }
        let url = URL(fileURLWithPath: path)
        return try? Data(contentsOf: url)
    }
    
}
