//
//  TodayViewController.swift
//  N26CodeChallengeTodayWidget
//
//  Created by a.danilyak on 31/03/2019.
//  Copyright © 2019 a.danilyak. All rights reserved.
//

import UIKit
import NotificationCenter
import N26CodeChallengeCore

final class TodayViewController: UIViewController, NCWidgetProviding {
        
    /// UI
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    
    /// Dependencies
    private lazy var apiService = APIService(configuration: APIServiceConfiguration.default)
    private lazy var exchangeRatesService = ExchangeRateService(apiService: apiService)
    
    /// Properties
    private var timer: Timer?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        priceLabel.text = "---"
        dateLabel.text = "---"
        
        update()
        startUpdate()
    }
    
    deinit {
        stopUpdate()
    }
    
    // MARK: - Update
    
    private func update() {
        exchangeRatesService.loadBitcoinExchangeRates { result in
            switch result {
            case .success(let rates):
                guard let eurRate = rates.first(where: { $0.to == .eur }) else { return }
                DispatchQueue.main.async { [weak self] in
                    self?.priceLabel.text = "\(eurRate.rate) \(eurRate.to.symbol ?? "")"
                    self?.dateLabel.text = DateFormatter.ddMMyyyyHHmm.string(from: eurRate.date)
                }
            case .failure:
                DispatchQueue.main.async { [weak self] in
                    self?.priceLabel.text = "---"
                    self?.dateLabel.text = "---"
                }
            }
        }
    }
    
    private func startUpdate() {
        timer = Timer.scheduledTimer(withTimeInterval: 10, repeats: true) { [weak self] _ in
            self?.update()
        }
    }
    
    private func stopUpdate() {
        timer?.invalidate()
        timer = nil
    }
        
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        completionHandler(NCUpdateResult.newData)
    }
}

// MARK: - Constants
private extension DateFormatter {
    
    static let ddMMyyyyHHmm: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy HH:mm"
        formatter.timeZone = Calendar.current.timeZone
        return formatter
    }()
}
