# N26 Code Challenge

App is written in Swift 5, so you need the latest Xcode 10.2.

The project uses CocoaPods as a dependency manager, so, before building project you need to run `pod install` in the project root directory.
The project uses 2 dependencies:
	* SwiftyJSON - lightweight simple librarty for dealing with JSONs, it saves time doing routine for you.
	* SnapKit - also lightweight simple library for dealing with Constraints. I like the syntax it provides.
	
Run `N26CodeChallenge` target to test the main iPhone app.
Run `N26CodeChallengeTodayWidget` target to test the today widget.